import tkinter as tk;
from tkinter import ttk;

class App:
	def __init__(self):
		self.win = tk.Tk();
		self.win.title("Teclado celular.");
		self.entText = tk.StringVar(value = "");
		self.ent1 = ttk.Entry(self.win, textvariable = self.entText);
		self.EntWidth = 250
		self.EntHeight = 30
		self.actNum = "";
		self.ent1.place(x = 2, y = 2, width = self.EntWidth, height = self.EntHeight);
		self.buttonCreation()
		self.win.geometry(str(self.EntWidth+5)+"x"+str(self.btHeight*4+self.EntHeight+10))
		self.win.resizable(0,0)
		self.win.mainloop()

	def btPress0(self):
		self.actNum = ""
		num = self.entText.get() + ' ';
		self.entText.set(num);

	def btPress1(self):
		self.actNum = ""
		num = self.entText.get()
		num = num[0:-1];
		self.entText.set(num);

	def btPress2(self):
		num = self.entText.get() 
		if ('3' in self.actNum or '4' in self.actNum or '5' in self.actNum or '6' in self.actNum 
			or '7' in self.actNum or '8' in self.actNum or '9' in self.actNum or len(self.actNum) == 0) :
			num += " "
			self.actNum = '2';
		elif len(self.actNum) == 3:
			self.actNum = '2';
		else:
			self.actNum += '2';

		if self.actNum == '2':
			num = num[0:-1]
			num += "A"
			self.entText.set(num);

		elif self.actNum == '22':
			num = num[0:-1]
			num += "B"
			self.entText.set(num);

		elif self.actNum == '222':
			num = num[0:-1]
			num += "C"
			self.entText.set(num);

	def btPress3(self):
		num = self.entText.get() 
		if ('2' in self.actNum or '4' in self.actNum or '5' in self.actNum or '6' in self.actNum 
			or '7' in self.actNum or '8' in self.actNum or '9' in self.actNum or len(self.actNum) == 0) :
			num += " "
			self.actNum = '3';
		elif len(self.actNum) == 3:
			self.actNum = '3';
		else:
			self.actNum += '3';

		if self.actNum == '3':
			num = num[0:-1]
			num += "D"
			self.entText.set(num);

		if self.actNum == '33':
			num = num[0:-1]
			num += "E"
			self.entText.set(num);

		if self.actNum == '333':
			num = num[0:-1]
			num += "F"
			self.entText.set(num);

	def btPress4(self):
		num = self.entText.get() 
		if ('2' in self.actNum or '3' in self.actNum or '5' in self.actNum or '6' in self.actNum 
			or '7' in self.actNum or '8' in self.actNum or '9' in self.actNum or len(self.actNum) == 0) :
			num += " "
			self.actNum = '4';
		elif len(self.actNum) == 3:
			self.actNum = '4';
		else:
			self.actNum += '4';

		if self.actNum == '4':
			num = num[0:-1]
			num += "G"
			self.entText.set(num);

		if self.actNum == '44':
			num = num[0:-1]
			num += "H"
			self.entText.set(num);

		if self.actNum == '444':
			num = num[0:-1]
			num += "I"
			self.entText.set(num);

	def btPress5(self):
		num = self.entText.get() 
		if ('2' in self.actNum or '3' in self.actNum or '4' in self.actNum or '6' in self.actNum 
			or '7' in self.actNum or '8' in self.actNum or '9' in self.actNum or len(self.actNum) == 0) :
			num += " "
			self.actNum = '5';
		elif len(self.actNum) == 3:
			self.actNum = '5';
		else:
			self.actNum += '5';

		if self.actNum == '5':
			num = num[0:-1]
			num += "J"
			self.entText.set(num);

		if self.actNum == '55':
			num = num[0:-1]
			num += "K"
			self.entText.set(num);

		if self.actNum == '555':
			num = num[0:-1]
			num += "L"
			self.entText.set(num);

	def btPress6(self):
		num = self.entText.get() 
		if ('2' in self.actNum or '3' in self.actNum or '4' in self.actNum or '5' in self.actNum 
			or '7' in self.actNum or '8' in self.actNum or '9' in self.actNum or len(self.actNum) == 0) :
			num += " "
			self.actNum = '6';
		elif len(self.actNum) == 4:
			self.actNum = '6';
		else:
			self.actNum += '6';

		if self.actNum == '6':
			num = num[0:-1]
			num += "M"
			self.entText.set(num);

		if self.actNum == '66':
			num = num[0:-1]
			num += "N"
			self.entText.set(num);

		if self.actNum == '666':
			num = num[0:-1]
			num += "Ñ"
			self.entText.set(num);

		if self.actNum == '6666':
			num = num[0:-1]
			num += "O"
			self.entText.set(num);

	def btPress7(self):
		num = self.entText.get() 
		if ('2' in self.actNum or '3' in self.actNum or '4' in self.actNum or '5' in self.actNum 
			or '6' in self.actNum or '8' in self.actNum or '9' in self.actNum or len(self.actNum) == 0) :
			num += " "
			self.actNum = '7';
		elif len(self.actNum) == 4:
			self.actNum = '7';
		else:
			self.actNum += '7';

		if self.actNum == '7':
			num = num[0:-1]
			num += "P"
			self.entText.set(num);

		if self.actNum == '77':
			num = num[0:-1]
			num += "Q"
			self.entText.set(num);

		if self.actNum == '777':
			num = num[0:-1]
			num += "R"
			self.entText.set(num)

		if self.actNum == '7777':
			num = num[0:-1]
			num += "S"
			self.entText.set(num);

	def btPress8(self):
		num = self.entText.get() 
		if ('2' in self.actNum or '3' in self.actNum or '4' in self.actNum or '5' in self.actNum 
			or '6' in self.actNum or '7' in self.actNum or '9' in self.actNum or len(self.actNum) == 0) :
			num += " "
			self.actNum = '8';
		elif len(self.actNum) == 3:
			self.actNum = '8';
		else:
			self.actNum += '8';

		if self.actNum == '8':
			num = num[0:-1]
			num += "T"
			self.entText.set(num);

		if self.actNum == '88':
			num = num[0:-1]
			num += "U"
			self.entText.set(num);

		if self.actNum == '888':
			num = num[0:-1]
			num += "V"
			self.entText.set(num)

	def btPress9(self):
		num = self.entText.get() 
		if ('2' in self.actNum or '3' in self.actNum or '4' in self.actNum or '5' in self.actNum 
			or '6' in self.actNum or '7' in self.actNum or '8' in self.actNum or len(self.actNum) == 0) :
			num += " "
			self.actNum = '9';
		elif len(self.actNum) == 4:
			self.actNum = '9';
		else:
			self.actNum += '9';

		if self.actNum == '9':
			num = num[0:-1]
			num += "W"
			self.entText.set(num);

		if self.actNum == '99':
			num = num[0:-1]
			num += "X"
			self.entText.set(num);

		if self.actNum == '999':
			num = num[0:-1]
			num += "Y"
			self.entText.set(num)

		if self.actNum == '9999':
			num = num[0:-1]
			num += "Z"
			self.entText.set(num)

	def btPressALM(self):
		self.actNum = ""
		num = self.entText.get() + '';
		self.entText.set(num);

	def btPressAST(self):
		self.actNum = ""
		self.entText.set("")
 

	def buttonCreation(self):
		self.btWidth = self.EntWidth/3;
		self.btHeight = 55;
		#ROW1
		self.bt1 = ttk.Button(self.win, text = "1 (DEL)", command = self.btPress1);
		self.bt1.place(x = 2, 				     y = 35+2, 					       width = self.btWidth-2, height = self.btHeight);

		self.bt2 = ttk.Button(self.win, text = "2(ABC)", command = self.btPress2);
		self.bt2.place(x = self.btWidth + 4,     y = 35+2, 					       width = self.btWidth-2, height = self.btHeight);

		self.bt3 = ttk.Button(self.win, text = "3(DEF)", command = self.btPress3);
		self.bt3.place(x = self.btWidth*2 + 5,   y = 35+2, 				   	       width = self.btWidth-2, height = self.btHeight);
		#ROW2
		self.bt4 = ttk.Button(self.win, text = "4(GHI)", command = self.btPress4);
		self.bt4.place(x = 2, 				     y = 35+2 + self.btHeight + 2,     width = self.btWidth-2, height = self.btHeight);

		self.bt5 = ttk.Button(self.win, text = "5(JKL)", command = self.btPress5);
		self.bt5.place(x = self.btWidth + 4,     y = 35+2 + self.btHeight + 2,     width = self.btWidth-2, height = self.btHeight);

		self.bt6 = ttk.Button(self.win, text = "6(MNÑO)", command = self.btPress6);
		self.bt6.place(x = self.btWidth*2 + 5,   y = 35+2 + self.btHeight + 2,     width = self.btWidth-2, height = self.btHeight);
		#ROW3
		self.bt7 = ttk.Button(self.win, text = "7(PQRS)", command = self.btPress7);
		self.bt7.place(x = 2, 				     y = 35+2 + self.btHeight*2 + 2,   width = self.btWidth-2, height = self.btHeight);

		self.bt8 = ttk.Button(self.win, text = "8(TUV)", command = self.btPress8);
		self.bt8.place(x = self.btWidth + 4,     y = 35+2 + self.btHeight*2 + 2,   width = self.btWidth-2, height = self.btHeight);

		self.bt9 = ttk.Button(self.win, text = "9(WXYZ)", command = self.btPress9);
		self.bt9.place(x = self.btWidth*2 + 5,   y = 35+2 + self.btHeight*2 + 2,   width = self.btWidth-2, height = self.btHeight);
		#ROW4
		self.btAST = ttk.Button(self.win, text = "*(clear)", command = self.btPressAST);
		self.btAST.place(x = 2, 			     y = 35+2 + self.btHeight*3 + 2,   width = self.btWidth-2, height = self.btHeight);

		self.bt0 = ttk.Button(self.win, text = "0 (space)", command = self.btPress0);
		self.bt0.place(x = self.btWidth + 4,     y = 35+2 + self.btHeight*3 + 2,   width = self.btWidth-2, height = self.btHeight);

		self.btALM = ttk.Button(self.win, text = "# (next)", command = self.btPressALM);
		self.btALM.place(x = self.btWidth*2 + 5, y = 35+2 + self.btHeight*3 + 2,   width = self.btWidth-2, height = self.btHeight);

#Execute:

App();


"""
GRIDS
.grid(column = 0, row = 0, padx = 2, pady = 2, columnspan = 3, sticky = "we");
"""