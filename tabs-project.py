#The variable names aren't very intuitive
#It was one of my firsts programs
import tkinter as tk
from tkinter import ttk
class App:
	def __init__(s):
		s.varCreation()

		s.page.append(ttk.Frame(s.noteBk))
		s.noteBk.add(s.page[0], text = "unnamed")
		s.lb.append(ttk.Label(s.page[0],text = "Enter a title:"))
		s.lb[0].grid(column = 0, row = 0)
		s.title.append(tk.StringVar())
		s.ent.append(ttk.Entry(s.page[0], textvariable = s.title[0]))
		s.ent[0].grid(column = 1, row = 0)
		s.bt.append(ttk.Button(s.page[0],text = "Update",command = s.titleChange).grid(column = 0,row = 1))
		
		s.noteBk.grid(column = 0, row = 0)

		s.generPage = (ttk.Frame(s.noteBk))
		s.noteBk.add(s.generPage, text = "+")
		s.btAdd = ttk.Button(s.generPage, text = "Add page.", command = s.createPage).grid(column = 0, row = 0)
		
		s.noteBk.bind_all("<Control-w>",s.closePage)
		s.noteBk.bind_all("<Control-W>",s.closePage)

		s.win.geometry("400x200")
		s.win.mainloop()
	def createPage(s):
		x = s.noteBk.index(tk.END)-1
		s.page.append(ttk.Frame(s.noteBk))
		s.noteBk.insert(x, s.page[x], text = "unnamed")
		s.lb.append(ttk.Label(s.page[x],text = "Enter a title:"))
		s.lb[x].grid(column = 0, row = 0)
		s.title.append(tk.StringVar())
		s.ent.append(ttk.Entry(s.page[x], textvariable = s.title[x]))
		s.ent[x].grid(column = 1, row = 0)
		s.bt1 = ttk.Button(s.page[x],text = "Update",command = s.titleChange).grid(column = 0,row = 1)

	def titleChange(s):
		num = s.noteBk.index(tk.CURRENT)
		if len(s.title[num].get())>0:
			s.noteBk.tab(tk.CURRENT, text = s.title[num].get())
	def closePage(s,event):
		num = s.noteBk.index(tk.CURRENT)
		addNum = s.noteBk.index(tk.END)-1
		if num != addNum:
			s.noteBk.forget(num)

	def varCreation(s):
		s.win = tk.Tk()
		s.noteBk = ttk.Notebook(s.win)
		s.title = []
		s.page = []
		s.lb = []
		s.ent = []
		s.bt = []

app = App()
