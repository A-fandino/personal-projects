#The texts are in spanish 
#The are some gramatical errors on the functions' names 
import tkinter as tk;
from tkinter import ttk;
from tkinter import Spinbox

class Bytes:
	def __init__(self):
		self.win = tk.Tk();
		self.win.title("Unidades de mesura");

		self.createfloaterface();
		self.win.resizable(0,0)
		self.win.mainloop();

	def createfloaterface(self):
		#Label Frame 1
		self.lbFrm1 = ttk.Labelframe(self.win, text = "Ingreso de datos:");
		self.lbFrm1.grid(column = 0, row  = 0, padx = 5, pady = 5);
		#Label 1
		self.lb1 = ttk.Label(self.lbFrm1, text = "floatroduce los datos: ");
		self.lb1.grid(column = 0, row  = 0, padx = 5, pady = 5);
		#Entry 1
		self.dataEnter = tk.StringVar(value = "10.0")
		self.ent1 = ttk.Entry(self.lbFrm1, textvariable = self.dataEnter, width = "25");
		self.ent1.grid(column = 1, row  = 0, padx = 5, pady = 5);
		#Combo Box Magnitudes Entrada
		magnitTuple = (" ", "k", "M", "G", "T", "P", "E", "Z", "Y");
		self.magnitud = tk.StringVar(value = "k");
		self.magnitudBox = ttk.Combobox(self.lbFrm1, textvariable = self.magnitud ,values = magnitTuple, width = "5");
		self.magnitudBox.grid(column = 2, row  = 0, padx = 5, pady = 5);
		#Combo Box Unidades Entrada
		self.unit = tk.StringVar(value = "B");
		self.unitBox = ttk.Combobox(self.lbFrm1, textvariable = self.unit ,values = ("B","iB"), width = "5");
		self.unitBox.grid(column = 3, row  = 0, padx = 5, pady = 5);
		#Boton 1
		self.bt1 = ttk.Button(self.lbFrm1, command = self.toBytes, text = "Ingresar");
		self.bt1.grid(column = 0, row  = 1, padx = 5, pady = 5);

		#****************************************************************************************************************#

		#Label Frame 2
		self.lbFrm2 = ttk.Labelframe(self.win, text = "Salida de datos");
		self.lbFrm2.grid(column = 0, row  = 1, padx = 5, pady = 5);
		#Label 2
		self.lb2 = ttk.Label(self.lbFrm2, text = "Elije la unidad y magnitud: ");
		self.lb2.grid(column = 0, row  = 0, padx = 5, pady = 5);
		#Entry 2
		self.dataOut = tk.StringVar()
		self.ent2 = ttk.Entry(self.lbFrm2, textvariable = self.dataOut, width = "25", state = "readonly");
		self.ent2.grid(column = 1, row  = 0, padx = 5, pady = 5);
		#Combo Box Magnitudes Salida
		self.magnitudOut = tk.StringVar(value = "k");
		self.magnitudBoxOut = ttk.Combobox(self.lbFrm2, textvariable = self.magnitudOut ,values = magnitTuple, width = "5");
		self.magnitudBoxOut.grid(column = 2, row  = 0, padx = 5, pady = 5);
		#Combo Box Unidades Salida
		self.unitOut = tk.StringVar(value = "iB");
		self.unitBoxOut = ttk.Combobox(self.lbFrm2, textvariable = self.unitOut ,values = ("B","iB"), width = "5");
		self.unitBoxOut.grid(column = 3, row  = 0, padx = 5, pady = 5);
		#Label 3
		self.lb3 = ttk.Label(self.lbFrm2, text = "Cantidad de decimales: ")
		self.lb3.grid(column = 0, row = 1, padx = 5, pady = 5)
		#Spinbox decimales
		self.numDec = tk.IntVar(value = 2)
		self.decSpin = Spinbox(self.lbFrm2, textvariable = self.numDec,from_ = 1, to = 20, state = "readonly", wrap = True, width = "5")
		self.decSpin.configure(state = "readonly")
		self.decSpin.grid(column = 1, row  = 1, padx = 5, pady = 5);


	def toBytes(self): 
		self.bytes = float(self.dataEnter.get())
		if self.magnitud.get() != " ":
			if self.unit.get() == "B":
				if self.magnitud.get() == "k":
					self.bytes *= 10**3
				elif self.magnitud.get() == "M":
					self.bytes *= 10**6
				elif self.magnitud.get() == "G":
					self.bytes *= 10**9
				elif self.magnitud.get() == "T":
					self.bytes *= 10**12
				elif self.magnitud.get() == "P":
					self.bytes *= 10**15
				elif self.magnitud.get() == "E":
					self.bytes *= 10**18
				elif self.magnitud.get() == "Z":
					self.bytes *= 10**21
				elif self.magnitud.get() == "Y":
					self.bytes *= 10**24

			if self.unit.get() == "iB":
				if self.magnitud.get() == "k":
					self.bytes *= 2**10
				elif self.magnitud.get() == "M":
					self.bytes *= 2**20
				elif self.magnitud.get() == "G":
					self.bytes *= 2**30
				elif self.magnitud.get() == "T":
					self.bytes *= 2**40
				elif self.magnitud.get() == "P":
					self.bytes *= 2**50
				elif self.magnitud.get() == "E":
					self.bytes *= 2**60
				elif self.magnitud.get() == "Z":
					self.bytes *= 2**70
				elif self.magnitud.get() == "Y":
					self.bytes *= 2**80
		if self.magnitudOut.get() != " ":
			self.transformBytes()

	def transformBytes(self):
		passVar = self.bytes
		if self.unitOut.get() == "B":
			if self.magnitudOut.get() == "k":
				passVar /= 10**3
			elif self.magnitudOut.get() == "M":
				passVar /= 10**6
			elif self.magnitudOut.get() == "G":
				passVar /= 10**9
			elif self.magnitudOut.get() == "T":
				passVar /= 10**12
			elif self.magnitudOut.get() == "P":
				passVar /= 10**15
			elif self.magnitudOut.get() == "E":
				passVar /= 10**18
			elif self.magnitudOut.get() == "Z":
				passVar /= 10**21
			elif self.magnitudOut.get() == "Y":
				passVar /= 10**24

		if self.unitOut.get() == "iB":
			if self.magnitudOut.get() == "k":
				passVar /= 2**10
			elif self.magnitudOut.get() == "M":
				passVar /= 2**20
			elif self.magnitudOut.get() == "G":
				passVar /= 2**30
			elif self.magnitudOut.get() == "T":
				passVar /= 2**40
			elif self.magnitudOut.get() == "P":
				passVar /= 2**50
			elif self.magnitudOut.get() == "E":
				passVar /= 2**60
			elif self.magnitudOut.get() == "Z":
				passVar /= 2**70
			elif self.magnitudOut.get() == "Y":
				passVar /= 2**80
		self.dataOut.set(str(round(passVar,self.numDec.get())))


Bytes();